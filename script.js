const puppeteer = require("puppeteer");

async function checkGaGlobal(name, url) {
  const browser = await puppeteer.launch();
  const page = await browser.newPage();

  const start = Date.now();

  try {
    await page.goto(url);

    // Wait for the variable gaGlobal to be available in the window with a timeout of 4 seconds
    await page.waitForFunction("window.gaGlobal !== undefined", {
      timeout: 4000,
    });

    const elapsed = Date.now() - start;
    console.log(`${name}: gaGlobal is available after ${elapsed} ms`);
  } catch (error) {
    const elapsed = Date.now() - start;
    console.log(
      `${name}: Timeout after ${elapsed} ms. gaGlobal is not available.`,
    );
  } finally {
    await browser.close();
  }
}

checkGaGlobal("consentIgnored", "https://cookie-banner-test.vercel.app/");
checkGaGlobal(
  "ConsentedAccepted",
  "https://cookie-banner-test.vercel.app/?privacybee-scanner=true",
);
