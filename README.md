# Description

Tests if the Google analytics is loaded on the page https://cookie-banner-test.vercel.app and prints out how long it takes for the variable to appear

# Installation

`npm install`

# Run

`node script.js`
